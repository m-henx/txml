# Software to cross reference two B&R visu textgroup XML
# files in order to create flat text files for
# documentation purposes...

import xml.etree.ElementTree as ET

# open XML files
tree1 = ET.parse('Errors.txtgrp')
tree2 = ET.parse('ErrorDescription.txtgrp')
error = tree1.getroot()
errorDesc = tree2.getroot()

# languages to process
languages = ['en', 'de', 'it', 'es', 'nl']

# read index maps
errorDict = {}
descDict = {}

for item in error.findall('{http://br-automation.co.at/AS/VC/Project}IndexMap'):
	for ix in item.iter():
		if ix.tag == '{http://br-automation.co.at/AS/VC/Project}Index':
			errorDict[ix.attrib['ID']] = ix.attrib['Value']

for item in errorDesc.findall('{http://br-automation.co.at/AS/VC/Project}IndexMap'):
	for ix in item.iter():
		if ix.tag == '{http://br-automation.co.at/AS/VC/Project}Index':
			descDict[ix.attrib['ID']] = ix.attrib['Value']

# index errors and descriptions on actual error number
errorNums = {}
for language in languages:
    errorNums[language] = {}            

    for item in error.findall('{http://br-automation.co.at/AS/VC/Project}TextLayer'):
        if item.attrib == {'LanguageId': language}:    
            for txt in item.iter():
                if txt.tag == '{http://br-automation.co.at/AS/VC/Project}Text':
                    errorNums[language][errorDict[txt.attrib['ID']]] = txt.attrib['Value']

descNums = {}
for language in languages:
    descNums[language] = {}            

    for item in errorDesc.findall('{http://br-automation.co.at/AS/VC/Project}TextLayer'):
        if item.attrib == {'LanguageId': language}:    
            for txt in item.iter():
                if txt.tag == '{http://br-automation.co.at/AS/VC/Project}Text':
                    descNums[language][descDict[txt.attrib['ID']]] = txt.attrib['Value']

# prepare dict of lists to hold texts
lst = {}
for language in languages:
    lst[language] = []
    
# populate lists based on english (fallback language)
for key, value in errorNums['en'].items():
    keyFormatted = f"{key:>5}"
    errTxtEn = errorNums['en'][key]
    try:
        descTxtEn = descNums['en'][key]
    except:
        descTxtEn = '';
    for language in languages:
        try:
            errTxt = errorNums[language][key]
        except:
            errTxt = errTxtEn
        try:
            descTxt = descNums[language][key]
        except:
            descTxt = descTxtEn
        if errTxt != '':
            txtLine = keyFormatted + ";" + errTxt
        else:
            txtLine = keyFormatted + ";" + errTxtEn
        if descTxt != '':
            txtLine += ';' + descTxt
        else:
            txtLine += ';' + descTxtEn
        lst[language].append(txtLine)

# sort and write text files
for language in languages:
    lst[language].sort()
    f = open(language + ".txt", "w")
    for txt in lst[language]:
        f.write(txt.lstrip()+"\n")
    f.close()
